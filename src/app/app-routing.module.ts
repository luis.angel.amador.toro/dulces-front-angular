import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { CartpageComponent } from './pages/cartpage/cartpage.component';
import { CheckoutpageComponent } from './pages/checkoutpage/checkoutpage.component';
import { CheckoutsuccespageComponent } from './pages/checkoutsuccespage/checkoutsuccespage.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { Page404Component } from './pages/page404/page404.component';
import { ProductViewPageComponent } from './pages/product-view-page/product-view-page.component';
import { StorepageComponent } from './pages/storepage/storepage.component';

const routes: Routes = [
  { path: 'home', component: HomepageComponent },
  { path: 'product-view', component: ProductViewPageComponent },
  { path: 'checkout', component: CheckoutpageComponent },
  { path: 'cart', component: CartpageComponent },
  { path: 'about', component: AboutPageComponent },
  { path: 'store', component: StorepageComponent },
  { path: 'checkout/success', component: CheckoutsuccespageComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './modules/material/material.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ParallaxComponent } from './components/parallax/parallax.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { Page404Component } from './pages/page404/page404.component';
import { FooterComponent } from './components/footer/footer.component';
import { StorePreviewComponent } from './components/store-preview/store-preview.component';
import { ProductViewPageComponent } from './pages/product-view-page/product-view-page.component';
import { ProductViewComponent } from './components/product-view/product-view.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartComponent } from './components/cart/cart.component';
import { CartpageComponent } from './pages/cartpage/cartpage.component';
import { ProductFilterPipe } from './pipes/cart-filter.pipe';
import { CategoriesComponent } from './components/categories/categories.component';
import { StoreComponent } from './components/store/store.component';
import { StorepageComponent } from './pages/storepage/storepage.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { CheckoutpageComponent } from './pages/checkoutpage/checkoutpage.component';
import { CheckoutSuccessComponent } from './components/checkout-success/checkout-success.component';
import { CheckoutsuccespageComponent } from './pages/checkoutsuccespage/checkoutsuccespage.component';
import { AboutComponent } from './components/about/about.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoaderComponent,
    ParallaxComponent,
    HomepageComponent,
    Page404Component,
    FooterComponent,
    StorePreviewComponent,
    ProductViewPageComponent,
    ProductViewComponent,
    CartComponent,
    CartpageComponent,
    ProductFilterPipe,
    CategoriesComponent,
    StoreComponent,
    StorepageComponent,
    CheckoutComponent,
    CheckoutpageComponent,
    CheckoutSuccessComponent,
    CheckoutsuccespageComponent,
    AboutComponent,
    AboutPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

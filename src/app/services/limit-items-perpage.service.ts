import { Injectable } from '@angular/core';
import { ProductRawDto } from '../models/product-raw.model';
import { ProductService } from './product-service.service';

@Injectable({
  providedIn: 'root'
})
export class LimitItemsPerpageService {

  constructor(private productService: ProductService) { }
  productList: ProductRawDto[] = [];
  totalPages: number = 0;
  pageSize: number = 8;

  limitItemsPerpage(): void {
    this.productService.getProducts().subscribe(res => {
      this.productList = res
      this.totalPages = this.productList.length / this.pageSize;
    })  
  }

  changePage(pagNumber: number){
    let startIndex = pagNumber * this.pageSize - 1;
    const elementList: ProductRawDto[] = [];
    for(let i = 0; i < this.pageSize; i++){
      elementList.push(this.productList[startIndex])
      startIndex++;
    }
    return elementList;
  }
}

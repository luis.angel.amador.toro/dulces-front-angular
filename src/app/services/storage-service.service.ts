import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private storageSubscription = new Subject<string>();
  constructor() {}
  watchStorage(): Observable<any> {
    return this.storageSubscription.asObservable();
  }

  setItem(key: string, data: any) {
    localStorage.setItem(key, data);
    this.storageSubscription.next('changed');
  }

  removeItem(key: string) {
    localStorage.removeItem(key);
    this.storageSubscription.next('changed');
  }
}

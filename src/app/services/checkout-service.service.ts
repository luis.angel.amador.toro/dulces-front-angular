import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment'
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class CheckoutService{
    constructor(
        private http : HttpClient
    ){

    }
    postClientInformation(shipmentDetails: any): Observable<any>{
        return this.http.post(environment.checkoutApi, shipmentDetails)
    }
} 
import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class ScrollTopService {
    constructor() {
    }

    goToTop(): void {
        window.scrollTo(0, 0);
    }
}
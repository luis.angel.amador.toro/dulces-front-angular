import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment'
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ProductService{
    constructor(
        private http : HttpClient
    ){

    }

    getProducts() : Observable<any>{
        return this.http.get(environment.productApi);
    }
    getCategories() : Observable<any>{
        return this.http.get(environment.categoryApi);
    }
    getProductById(id: number): Observable<any>{
        return this.http.get(environment.productApi + '/' + `${id}`)
    }
} 
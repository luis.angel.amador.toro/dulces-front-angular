import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from '../../environments/environment'
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class PostalService{
    constructor(
        private http : HttpClient
    ){

    }
    getAddressByCP(cp: string): Observable<any>{
        return this.http.get(environment.postalApi + '/' + `${cp}`)
    }
} 
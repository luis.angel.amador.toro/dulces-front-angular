import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { ProductDto } from '../models/product.model';
import { ProductService } from './product-service.service';
import { StorageService } from './storage-service.service';

@Injectable({
  providedIn: 'root',
})
export class AddToCartService {
  constructor(
    private productService: ProductService,
    private storageService: StorageService
  ) {}
  carrito: ProductDto[] = [];
  producto: any = {};

  getCartFromLocalStorage(): void {
    const currentCart = localStorage.getItem('carrito') ?? '[]';
    this.carrito = JSON.parse(currentCart);
  }

  setCartToLocalStorage(): void {
    this.storageService.setItem('carrito', JSON.stringify(this.carrito));
  }
  sendProductViewToCart(productDto : ProductDto){
    this.getCartFromLocalStorage()
    let existInCart = this.carrito.find((x) => x.id === productDto.id)
    if(existInCart){
      let newQuantity = productDto.quantity
      existInCart.quantity = newQuantity
      this.setCartToLocalStorage();
      Swal.fire(
        'Éxito',
        'El producto se actualizó en el carrito de compras',
        'success'
      );
      return;
    }
    
    this.producto.id = productDto.id;
    this.producto.name = productDto.name;
    this.producto.price = productDto.price;
    this.producto.imageUrl = productDto.imageUrl;
    this.producto.quantity = productDto.quantity;
    this.initializeCartDtoModel(this.producto);
   }
  sendProductToCart(id: number): void {
    this.getCartFromLocalStorage();

    this.productService.getProductById(id).subscribe((res) => {
      const existInCart = this.carrito.find((x) => x.id === id);
      if (existInCart) {
        const newQuantity = existInCart.quantity + 1;
        if (newQuantity > res.stock) {
          Swal.fire(
            'Error',
            'No se cuenta con stock suficiente para cubrir',
            'error'
          );
          return;
        }
        existInCart.quantity = newQuantity;
        this.setCartToLocalStorage();
        Swal.fire(
          'Éxito',
          'El producto se actualizó en el carrito de compras',
          'success'
        );
        return;
      }
      this.producto.id = res.id;
      this.producto.name = res.name;
      this.producto.price = res.price;
      this.producto.imageUrl = res.imageUrl;
      this.producto.quantity = 1;
      this.initializeCartDtoModel(this.producto);
    });
  }
  initializeCartDtoModel(product: ProductDto): void {
    this.getCartFromLocalStorage();
    this.carrito.push(product);
    this.setCartToLocalStorage();
    Swal.fire(
      'Éxito',
      'El producto se añadió al carrito de compras',
      'success'
    );
  }
}

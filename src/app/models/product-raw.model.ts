export interface ProductRawDto {
    [x: string]: any; 
    id: number;
    name: string;
    description: string;
    category: {};
    tags: string;
    imageUrl: string;
    price: number;
    stock: number;
}
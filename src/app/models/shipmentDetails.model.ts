import { ProductDto } from "./product.model";

export interface ChekoutDetailsDto{ 
    name: string;
    lastName: string;
    phone: string;
    shipment: string;
    email: string;
    rfc: string;
    cp: string;
    state: string;
    city: string;
    settlement: string;
    adress: string;
    total: number;
    notes: string;
    isPaid: number;
    cartProducts: ProductDto[]
}
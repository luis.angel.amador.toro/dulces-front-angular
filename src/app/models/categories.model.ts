export interface CategoryDto { 
    id: number;
    name: string;
    urlImage: string;
}
export interface ProductDto { 
    id: number;
    name: string;
    imageUrl: string;
    price: number;
    quantity: number;
}
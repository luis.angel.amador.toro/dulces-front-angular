import { Component, OnInit } from '@angular/core';
import { CategoryDto } from 'src/app/models/categories.model';
import { ProductService } from 'src/app/services/product-service.service';
import { StorageService } from 'src/app/services/storage-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private storageService: StorageService, private productService: ProductService) {}

  localCart: any = localStorage.getItem('carrito');
  parsedLocalCart: number = JSON.parse(this.localCart);
  productsInShoppingCart: number = 0;
  categories: CategoryDto[] = []
  ngOnInit(): void {
    if (this.localCart != null) {
      this.productsInShoppingCart = JSON.parse(this.localCart).length;
    }
    this.getCategories();
    this.getLocalCartBySubscription();
  }
  getCategories() {
    this.productService.getCategories().subscribe(res => {
      this.categories = res
    })
  }
  getLocalCartBySubscription(): void {
    this.storageService.watchStorage().subscribe((data: string) => {
      const localCart = localStorage.getItem('carrito') ?? '[]';
      this.productsInShoppingCart = JSON.parse(localCart).length;
    });
  }
}

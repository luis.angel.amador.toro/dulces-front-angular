import { Component, OnInit } from '@angular/core';
import { ScrollTopService } from 'src/app/services/scrolll-top-service.service';
import { StorageService } from 'src/app/services/storage-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  constructor(private storageService: StorageService, private scrollService: ScrollTopService) {}
  productos: any;
  localCart: any;
  cart: any = localStorage.getItem('carrito');

  ngOnInit(): void {
    this.getCartItems();
  }
  scrollTop(): void {
    this.scrollService.goToTop()
  }
  removeItemFromCart(id:number): void {
    if(this.productos){
      this.productos.forEach((value: { id: number; }, index: any) =>{
        if(value.id === id) this.productos.splice(index, 1);
        this.localCart = this.productos
        this.storageService.setItem('carrito', JSON.stringify(this.localCart))
      })
    }
  }
  getCartItems(): void {
    if (this.cart) {
      this.productos = JSON.parse(this.cart);
    }
  }
  deleteCart(): void {
    if(this.productos.length > 0) {
      Swal.fire({
        title: '¿Estás seguro de borrar el carrito?',
        text: 'Deberás volver a añadir los productos a tu carrito',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        cancelButtonText: 'Cancelar',
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire('¡Éxito!', 'Tu carrito se ha eliminado', 'success');
          this.localCart = []
          this.productos = this.localCart
          this.storageService.setItem('carrito', JSON.stringify(this.localCart))
        }
      });
    }else{
      Swal.fire('Error', 'No tienes productos en tu carrito de compras', 'error')
    }
  }
  deleteProductFromCart(id: number): void{
    Swal.fire({
      title: '¿Estás seguro de borrar el producto?',
      text: 'Puedes volver a agregar el producto desde la tienda nuevamente',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.removeItemFromCart(id);
        Swal.fire('¡Éxito!', 'Tu producto ha sido removido del carrito', 'success');
      }
    });
  }
}

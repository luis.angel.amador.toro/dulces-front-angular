import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product-service.service';
import Swal from 'sweetalert2';
import { AddToCartService } from 'src/app/services/add-to-cart-service.service';
import { ProductRawDto } from 'src/app/models/product-raw.model';
import { ProductFilterPipe } from 'src/app/pipes/cart-filter.pipe';
import { ScrollTopService } from 'src/app/services/scrolll-top-service.service';

@Component({
  selector: 'app-store-preview',
  templateUrl: './store-preview.component.html',
  styleUrls: ['./store-preview.component.scss']
})
export class StorePreviewComponent implements OnInit {

  public page: number = 0
  constructor(
    private productService : ProductService,
    public cartService : AddToCartService,
    private scrollService : ScrollTopService
  ) { 
    this.getProducts()
  }
    productos: ProductRawDto[] = [];
    isLoading = true;

  ngOnInit(): void {
  }
  scrollTop(): void {
    this.scrollService.goToTop()
  }
  getProducts(): void {
    this.productService.getProducts()
    .subscribe((res) => {
      this.productos = res
      this.isLoading = false
    },
    error => {
      Swal.fire('Error', 'error al obtener los productos', 'error')
    })
  }
  sendProductToCart(id: number): void {
    this.cartService.sendProductToCart(id)
  }
  nextPage(): void {
    this.page += 8
  }
  previousPage(): void {
    if(this.page > 0){
      this.page -= 8
    }
  }
  
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddToCartService } from 'src/app/services/add-to-cart-service.service';
import { ProductService } from 'src/app/services/product-service.service';
import { ScrollTopService } from 'src/app/services/scrolll-top-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss']
})
export class StoreComponent implements OnInit {

  constructor(private route: ActivatedRoute, private productService: ProductService, private router: Router, private cartService: AddToCartService, private scrollService: ScrollTopService) {
    this.router.routeReuseStrategy.shouldReuseRoute = (() => { return false;})
   }
  categoria: string = ''
  isLoading: boolean = true
  products: any;
  resCategoria: any;
  ngOnInit(): void {
    this.getProductsByCategory();
  }
  getUrlParams():void{
    this.route.queryParams.subscribe(params => {
      this.categoria = params['category']
    })
  }
  sendProductToCart(id: number): void {
    this.cartService.sendProductToCart(id)
  }
  scrollTop(): void {
    this.scrollService.goToTop();
  }
  getProductsByCategory(): void{  
    this.getUrlParams()
    if(this.categoria){
      this.productService.getCategories().subscribe(result => {
        this.resCategoria = result
        const categoria = this.resCategoria.find((x: { name: string; }) => x.name === this.categoria)
        if(categoria){
          this.products = categoria.products
          this.isLoading = false
        }
        console.log(this.categoria)
      })
    }else{
      this.productService.getProducts().subscribe(res => {
        this.products = res
        this.isLoading = false
      })
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductDto } from 'src/app/models/product.model';
import { AddToCartService } from 'src/app/services/add-to-cart-service.service';
import { ProductService } from 'src/app/services/product-service.service';
import { ScrollTopService } from 'src/app/services/scrolll-top-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss'],
})
export class ProductViewComponent implements OnInit {
  constructor(private route: ActivatedRoute, public product: ProductService, private router: Router, private scrollTopService: ScrollTopService, private addToCartService: AddToCartService) {}

  id: number = 0;
  productElement: any;
  cantidad: number = 1;
  stock: number = 0;
  isLoading: boolean = true;
  currentProduct: ProductDto = {
    id: 0,
    name: '',
    imageUrl: '',
    price: 0,
    quantity: 0
  }
  ngOnInit(): void {
    this.getUrlParams();
    this.getProductByUrl();
  }
  getUrlParams(): void {
    this.route.queryParams.subscribe((params) => {
      this.id = params['id'];
    });
  }
  setCurrentProduct(): void {
    this.currentProduct.id = this.productElement.id
    this.currentProduct.imageUrl = this.productElement.imageUrl
    this.currentProduct.name = this.productElement.name
    this.currentProduct.price = this.productElement.price
    this.currentProduct.quantity = this.cantidad
  }
  addProductToCart(): void {
    this.setCurrentProduct()
    this.addToCartService.sendProductViewToCart(this.currentProduct)
  }
  buyProduct(): void {
    this.addProductToCart();
    Swal.fire('Éxito', 'Se agregó el producto correctamente', 'success')
    .then(() => {
      this.router.navigate(['/cart'])
      this.scrollTop();
    })
  }
  scrollTop(): void {
    this.scrollTopService.goToTop()
  }
  getProductByUrl(): void {
    this.product.getProductById(this.id).subscribe((res) => {
      this.productElement = res;
      this.stock = res.stock;
      this.isLoading = false;
    },
    error => {
      Swal.fire('Error', 'Error, producto no encontrado', 'error')
      .then(() => {
        this.router.navigate(['/home']);
        
      })
    });
  }
  addProductQuantity(): void {
    if (this.cantidad < this.stock) {
      this.cantidad++;
    }
  }
  removeProductQuantity(): void {
    if (this.cantidad > 1) {
      this.cantidad--;
    }
  }
}

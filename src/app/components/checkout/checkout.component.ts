import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChekoutDetailsDto } from 'src/app/models/shipmentDetails.model';
import { CheckoutService } from 'src/app/services/checkout-service.service';
import { PostalService } from 'src/app/services/postal-code.service';
import { ScrollTopService } from 'src/app/services/scrolll-top-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit {
  constructor(
    private postalService: PostalService,
    private router: Router,
    private checkoutService: CheckoutService,
    private scrollService: ScrollTopService,
  ) {}
  cp: string = '';
  valid: boolean = true;
  fields: any[] = [];
  estado: string = '';
  municipio: string = '';
  listaColonias: any[] = [];
  colonia: string = '';
  rfc: string = '';
  nombres: string = '';
  apellidos: string = '';
  carrito: any = localStorage.getItem('carrito');
  productos: any;
  productosEnCarrito: number = 0;
  numeroTelefono: string = '';
  envio: any = 0;
  total: number = 0;
  totalAlternativo: number = 0;
  costoEnvio: number = 0;
  email: string = '';
  direccion: string = '';
  notas: any = '';
  envio2: string = '';
  disabled: boolean = false;
  loadingSpinner: boolean = false;
  detallesEnvio: any = [
    {
      id: 1,
      envio: 'Presencial (Solo CDMX Y EDOMEX)',
      precio: 0,
    },
    {
      id: 2,
      envio: 'Envio nacional (FEDEX)',
      precio: 250,
    },
    {
      id: 3,
      envio:
        'Domicilio el costo varia según el repartidor (SOLO CDMX Y EDOMEX)',
      precio: 150,
    },
  ];

  ngOnInit(): void {
    this.getCartItemsFromLocalStorage();
    if (!this.productos) {
      Swal.fire(
        'Error',
        'No cuentas con ningún producto en el carrito',
        'error'
      ).then(() => {
        this.router.navigate(['/home']);
      });
    }
    if (this.productos.length < 1) {
      Swal.fire(
        'Error',
        'No cuentas con ningún producto en el carrito',
        'error'
      ).then(() => {
        this.router.navigate(['/home']);
      });
    }
    if (this.productos != null) {
      this.productosEnCarrito = this.productos.length;
      this.getTotalOfProducts();
      this.totalAlternativo = this.total;
    }
  }
  setUserData(): void {
    this.validateForm();
    if (this.valid === true) {
      Swal.fire({
        title: '¿Estás seguro de los datos?',
        text: 'No podrás editar tu información después',
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirmar pedido',
        cancelButtonText: 'Cancelar',
      }).then((result) => {
        if (result.isConfirmed) {
          this.disabled = true;
          this.loadingSpinner = true;
          Swal.fire(
            'Pedido confirmado',
            'En seguida te mostraremos como pagar',
            'success'
          ).then(() => {
            let productosCarrito = localStorage.getItem('carrito') ?? '[]';
            let productos = JSON.parse(productosCarrito);
            let shipmentDetails: ChekoutDetailsDto = {
              name: this.nombres,
              lastName: this.apellidos,
              phone: this.numeroTelefono,
              shipment: this.envio2,
              email: this.email,
              rfc: this.rfc,
              cp: this.cp,
              state: this.estado,
              city: this.municipio,
              settlement: this.colonia,
              adress: this.direccion,
              total: this.total,
              notes: this.notas,
              isPaid: 0,
              cartProducts: productos,
            };
            this.checkoutService
              .postClientInformation(shipmentDetails)
              .subscribe(
                (data) => {
                  let name = data.fullName;
                  let email = data.email;
                  let phone = data.phone;
                  let isPaid = data.isPaid;
                  this.router.navigate(['/checkout/success'], {
                    queryParams: {
                      name: `${name}`,
                      email: `${email}`,
                      phone: `${phone}`,
                      isPaid: `${isPaid}`,
                      total: `${this.total}`,
                    },
                  });
                  this.scrollService.goToTop();
                  localStorage.setItem('carrito', '[]');
                },
                (err) => {
                  Swal.fire('Error', err, 'error');
                }
              );
          });
        }
      });
    } else {
      let fields = '';
      this.fields.forEach((element) => {
        fields += element + ', ';
      });
      Swal.fire(
        'Error, campos faltantes:',
        `${fields.substring(0, fields.length - 2)}`,
        'error'
      );
      this.fields = [];
      this.valid = true;
    }
  }
  updateShipmentPrice(id: any): void {
    let detallesId = parseInt(id);
    let detalles = this.detallesEnvio.find(
      (x: { id: any }) => x.id === detallesId
    );
    this.costoEnvio = parseInt(detalles.precio);
    this.envio2 = detalles.envio;
    let costo = this.costoEnvio;
    let total = this.total;
    if (total === this.totalAlternativo) {
      this.total = total + costo;
    } else if (costo === 0) {
      this.total = this.totalAlternativo;
    } else if (total !== this.totalAlternativo) {
      this.total = this.totalAlternativo + costo;
    }
  }
  getTotalOfProducts(): void {
    let total = 0;
    for (const x of this.productos) {
      let itemTotal = x.quantity * x.price;
      total += itemTotal;
      this.total = total;
    }
  }
  getCartItemsFromLocalStorage(): void {
    if (this.carrito) {
      this.productos = JSON.parse(this.carrito);
    }
  }
  validateEmailFormat(email: string) {
    let stringAt = email.split('@');
    if (stringAt.length > 1) {
      let stringDot = stringAt[1].split('.');
      if (stringDot.length > 1) {
        return true;
      }
      return false;
    }
    return false;
  }
  validateForm() {
    if (!this.nombres) {
      this.valid = false;
      this.fields.push('Nombre(s)');
    }
    if (!this.apellidos) {
      this.valid = false;
      this.fields.push('Apellido(s)');
    }
    if (this.envio == 0) {
      this.valid = false;
      this.fields.push('Envío');
    }
    if (!this.numeroTelefono) {
      this.valid = false;
      this.fields.push('Telefono');
    }
    if (!this.email) {
      this.valid = false;
      this.fields.push('Email');
    } else {
      if (!this.validateEmailFormat(this.email)) {
        this.valid = false;
        this.fields.push('Email incorrecto');
      }
    }
    if (this.cp.length < 5) {
      this.valid = false;
      this.fields.push('CP');
    }
    if (!this.estado) {
      this.valid = false;
      this.fields.push('Estado');
    }
    if (!this.municipio) {
      this.valid = false;
      this.fields.push('Municipio');
    }
    if (!this.colonia) {
      this.valid = false;
      this.fields.push('Colonia');
    }
    if (!this.direccion) {
      this.valid = false;
      this.fields.push('Dirección');
    }
    if (!this.rfc) {
      this.valid = false;
      this.fields.push('RFC');
    }
  }
  showInfo(): void {
    Swal.fire({
      title: '<strong>¿Cómo funciona?</strong>',
      icon: 'info',
      html:
        '<a>Introduce tu codigo postal y se llenaran automaticamente los datos</a>' +
        '<br>' +
        '<a><b>¿Desconoces tu codigo postal? </b></a>' +
        '<a href="https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/Descarga.aspx" target="_blank" rel="noreferrer">Click aquí</a> ',
      showCloseButton: true,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Genial!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
    });
  }
  getAddressDataByCp(cp: string): void {
    if (cp.length == 5) {
      this.postalService.getAddressByCP(cp).subscribe((res) => {
        if (res.length > 0) {
          this.listaColonias = res;
          this.colonia = this.listaColonias[0];
          this.estado = res[0].estado;
          this.municipio = res[0].municipio;
        } else {
          Swal.fire('Error', 'Error al cargar los datos de dirección', 'error');
        }
      });
    }
    if (cp.length < 5) {
      this.listaColonias = [];
      this.colonia = '';
      this.estado = '';
      this.municipio = '';
    }
  }
}

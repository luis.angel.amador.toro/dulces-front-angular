import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-checkout-success',
  templateUrl: './checkout-success.component.html',
  styleUrls: ['./checkout-success.component.scss']
})
export class CheckoutSuccessComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }
  name: string = "";
  phone:string = "";
  email:string = "";
  isPaid:string = "";
  total:string = "";

  ngOnInit(): void {
    this.getUrlParams();
  }
  getUrlParams(): void{
    this.route.queryParams.subscribe((params) => {
      this.name = params['name'];
      this.phone = params['phone'];
      this.email = params['email'];
      this.total = params['total'];
      let isPaid = params['isPaid']
      if(isPaid == 0){
        this.isPaid = "Sin pagar"
      };
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { CategoryDto } from 'src/app/models/categories.model';
import { ProductService } from 'src/app/services/product-service.service';
import { ScrollTopService } from 'src/app/services/scrolll-top-service.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  constructor(private productService: ProductService, private scrollService: ScrollTopService) {}
  categories: CategoryDto[] = [];
  isLoading = true;

  ngOnInit(): void {
    this.loadCategories()
  }
  scrollTop(): void {
    this.scrollService.goToTop();
  }
  loadCategories() {
    this.productService.getCategories().subscribe((res) => {
      this.categories = res
      this.isLoading = false;
    });
  }
}

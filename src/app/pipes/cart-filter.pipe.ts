import { Pipe, PipeTransform } from '@angular/core';
import { ProductRawDto } from '../models/product-raw.model';

@Pipe({
  name: 'productFilter'
})
export class ProductFilterPipe implements PipeTransform {

  transform(products: ProductRawDto[], page: number): ProductRawDto[] {
    return products.slice(page, page + 8);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutsuccespageComponent } from './checkoutsuccespage.component';

describe('CheckoutsuccespageComponent', () => {
  let component: CheckoutsuccespageComponent;
  let fixture: ComponentFixture<CheckoutsuccespageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutsuccespageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutsuccespageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const baseApi = '/back'
export const environment = {
  production: true,
  productApi: `${baseApi}/api/productos`,
  categoryApi: `${baseApi}/api/category`
};
